const request = require('request');
const https = require('https');
const express = require('express');
const app = express();
const bcrypt = require('bcryptjs');

var nombre;
var apellido;
var saldo_mxn;
var saldo_btc;
var ult_precio;
//var usrname;
var passwd;

// getUserInfo("juancho").then((res)=>{
//   console.log("res promesa: ");
//   console.log(res);
// });

// comprarYGuardar("juancho",3).then((resulta)=>{
//   console.log(resulta);
// },(erro)=>{
//   console.log(erro);
// });

// venderYGuardar("juancho",3).then((resulta)=>{
//   console.log(resulta);
// },(erro)=>{
//   console.log(erro);
// });

// guardarSaldos("juancho","1000000.04","0.00001","155000.01").then((res)=>{
//   console.log("pusss promesa positiva");
//   console.log(JSON.parse(JSON.stringify(res.matchedCount)).$numberInt);
// },(error)=>{
//   console.log("Error: "+error);
// });
//console.log(usrname);

function comprarYGuardar(username,unid){
  var resultSaldo;
  var resultBtc;
  var resultGuardar;
  var hayError=false;
  var unidades = parseFloat(unid);

    return new Promise((res,rej)=>{
      getUserInfo(username).then((result)=>{
        //return result;
        //resultSaldo = parseFloat(result);
        console.log("res de GetUserInfo");
        console.log(result);
        console.log("tengo nombre: "+nombre);
        console.log("tengo apellido: "+apellido);
        console.log("tengo ultimo precio: "+ult_precio);
        console.log("tengo saldo mxn: "+saldo_mxn);
        console.log("tengo saldo btc: "+saldo_btc);
        getBtcPrecio().then((result2)=>{
          console.log("res de getBtcPrecio");
          resultBtc = parseFloat(result2);
          if(resultBtc*unidades > saldo_mxn){
            console.log("no joveeen");
            rej("Fondos insuficientes");
          }else{
            var totalBtcs = parseFloat(unidades*resultBtc);
            var loquequeda = parseFloat(saldo_mxn-totalBtcs);
            var nuevoSaldoBtc = saldo_btc + unidades;
            console.log("Cartera mxn: "+saldo_mxn+" de tipo: "+typeof(saldo_mxn));
            console.log("Unidades de btc: "+ unidades+" de tipo: "+typeof(unidades));
            console.log("precio unitario: "+resultBtc+" de tipo: "+typeof(resultBtc));
            console.log("precio total: "+totalBtcs+" de tipo: "+typeof(totalBtcs));
            console.log("cartera mxn restante: "+loquequeda+" de tipo: "+typeof(loquequeda));
            guardarSaldos(username,loquequeda.toString(),nuevoSaldoBtc.toString(),resultBtc.toString()).then((resulta)=>{
              console.log("res de guardarSaldos");
              res("Operacion exitosa");
              //VALIDAR QUE SI SE HAYAN GUARDADO SALDOS
              console.log("SALDOS GUARDADOS");
            },(error3)=>{
              rej("Error al guardar saldos");
            });
          }
        },(error2)=>{
          rej("Erro al ");
        });
      },(error)=>{
        rej("Error al ");
      });
  });
}

function venderYGuardar(username,unidades){
  var resultSaldo;
  var resultBtc;
  var resultGuardar;
  var hayError=false;

    return new Promise((res,rej)=>{
      getUserInfo(username).then((result)=>{
        //return result;
        //resultSaldo = parseFloat(result);
        console.log("res de GetUserInfo");
        console.log(result);
        console.log("tengo nombre: "+nombre);
        console.log("tengo apellido: "+apellido);
        console.log("tengo ultimo precio: "+ult_precio);
        console.log("tengo saldo mxn: "+saldo_mxn);
        console.log("tengo saldo btc: "+saldo_btc);
        getBtcPrecio().then((result2)=>{
          console.log("res de getBtcPrecio");
          resultBtc = parseFloat(result2);
          if(unidades > saldo_btc){
            console.log("no joveeen");
            rej("Fondos insuficientes");
          }else{
            var totalMxns = parseFloat(unidades*resultBtc);
            var loquequeda = parseFloat(saldo_btc-unidades);
            var nuevoSaldoMxn = saldo_mxn + totalMxns;
            console.log("nueva cartera mxn: "+nuevoSaldoMxn+" de tipo: "+typeof(nuevoSaldoMxn));
            console.log("Unidades de btc vendidas: "+ unidades+" de tipo: "+typeof(unidades));
            console.log("precio unitario: "+resultBtc+" de tipo: "+typeof(resultBtc));
            console.log("venta total: "+totalMxns+" de tipo: "+typeof(totalMxns));
            console.log("cartera btc restante: "+loquequeda);
            guardarSaldos(username,nuevoSaldoMxn.toString(),loquequeda.toString(),resultBtc.toString()).then((resulta)=>{
              console.log("res de guardarSaldos");
              res("Operacion exitosa");
              //VALIDAR QUE SI SE HAYAN GUARDADO SALDOS
              console.log("SALDOS GUARDADOS");
            },(error3)=>{
              rej("Error al guardar saldos");
            });
          }
        },(error2)=>{
          rej("Erro al ");
        });
      },(error)=>{
        rej("Error al ");
      });
  });
}

function getBtcPrecio(){
  console.log("entro a precioBTc");
  let url = `https://api.bitso.com/v3/ticker/?book=btc_mxn`;

  return new Promise((res,rej)=>{
    try{
      let req = request({
        url : url,
        json : true
      }, (error,response,body)=>{
        console.log("precio BTC: "+JSON.parse(JSON.stringify(response)).body.payload.last);
        // return (JSON.parse(JSON.stringify(response)).body.payload.last);
        res(JSON.parse(JSON.stringify(response)).body.payload.last);
      });

    }catch(err){
      console.log("Reject de precio Btc");
      rej(0);
    }
  });
}

function getUserInfo(username){
  let url3 = `https://webhooks.mongodb-stitch.com/api/client/v2.0/app/btcinvestor-lxwtb/service/getusers/incoming_webhook/getUsers?usrname=${username}`;
  let obj;
  return new Promise((res,rej)=>{
    let req3 = request({
      url : url3,
      json : true
    }, (error,response,body)=>{
      obj = JSON.parse(JSON.stringify(body));
      nombre = obj.nombre;
      apellido = obj.apellido;
      // ult_precio = parseFloat(obj.ultimo_precio.$numberDecimal);
      // saldo_mxn = parseFloat(obj.saldo_mxn.$numberDouble);
      // saldo_btc = parseFloat(obj.saldo_btc.$numberDouble);
      ult_precio = parseFloat(obj.ultimo_precio);
      saldo_mxn = parseFloat(obj.saldo_mxn);
      saldo_btc = parseFloat(obj.saldo_btc);
      passwd = obj.password;
      res(obj);
    //  return JSON.parse(JSON.stringify(body)).$numberDecimal;
      //return (`usrname:${error}`);
    });
  });
}

function guardarSaldos(username1, saldoMxn1, saldoBtc1, ultimoPrecio1){
  //var CHROME_USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like gecko) Chrome/41.0.2228.o Safari/537.36"

  var data = JSON.stringify({
    username:username1,
    saldoMxn:saldoMxn1,
    saldoBtc:saldoBtc1,
    ultimoPrecio:ultimoPrecio1
  });

  var options = {
    host: 'webhooks.mongodb-stitch.com',
    path: `/api/client/v2.0/app/btcinvestor-lxwtb/service/setSaldos/incoming_webhook/setSaldos?username=${username1}&saldoMxn=${saldoMxn1}&saldoBtc=${saldoBtc1}&ultimoPrecio=${ultimoPrecio1}`,
    method: 'PUT',
    headers: {
    'Content-Type': 'application/json',
    'Content-Length': data.length
    }
  };

  return new Promise((res,rej)=>{
    var request = https.request(options, (response)=>{
      var body = '';
      //console.log("intento hacer request");
      response.on('data',(out)=>{
        //console.log("intento procesar");
        console.log("status: "+response.statusCode);
        body += out;
      });

      response.on('end',(out)=>{
        //console.log(response);
        var json = JSON.parse(body);
        console.log(body);
        console.log(json);
        res(json);
      });
    });

    request.on('error', (e)=>{
      console.log(e);
      rej(e);
    });

    request.write(data);
    request.end();
  });
}

function setUser(username2, nombre2, apellido2, password2){
  console.log("Entro a setUSer con los sig params:");
  console.log("username: "+username2);
  console.log("name: "+nombre2);
  console.log("apellido: "+apellido2);
  console.log("passwd: "+password2);


  return new Promise((res,rej)=>{
    bcrypt.genSalt(10,function(err,salt){
      bcrypt.hash(password2, salt, function(err,hash){
        console.log("hashedP: "+hash);

        var data = JSON.stringify({
          username:username2,
          name:nombre2,
          apellido:apellido2,
          passwd:hash
        });

        var options = {
          host: 'webhooks.mongodb-stitch.com',
          path: `/api/client/v2.0/app/btcinvestor-lxwtb/service/setUser/incoming_webhook/setUser?username=${username2}&name=${nombre2}&apellido=${apellido2}&passwd=${hash}`,
          method: 'POST',
          headers: {
          'Content-Type': 'application/json',
          'Content-Length': data.length
          }
        };

        var request = https.request(options, (response)=>{
          var body = '';

          response.on('data',(out)=>{
            body += out;
          });

          response.on('end',(out)=>{
            var json = JSON.parse(body);
            res(json);
          });
        });

        request.on('error', (e)=>{
          rej(e);
        });

        request.write(data);
        request.end();
      });
    });
  });
}

app.get('/cliente/:username',(req,res)=>{
  getUserInfo(req.params.username).then((resulta)=>{
    res.send(resulta);
  },(erro)=>{
    res.send(erro);
  });
});

app.get('/login/:username/:password',(req,res)=>{
  getUserInfo(req.params.username).then((resulta)=>{
    bcrypt.compare(req.params.password,resulta.password,function(err,resp){
      console.log(req.params.password);
      console.log(resulta.password);
      console.log(resp);
      if(resp === true){
        res.send("Login correcto");
      }
      else{
        res.send(err);
      }
    });
    // res.send(resulta);
  },(erro)=>{
    res.send(erro);
  });
});

app.post('/nuevo/:username/:nombre/:apellido/:password',(req,res)=>{
  setUser(req.params.username,req.params.nombre,req.params.apellido,req.params.password).then((result)=>{
    res.send(result);
  },(err)=>{
    res.send(err);
  });
});

app.put('/compra/:username/:unidades',(req,res)=>{
  comprarYGuardar(req.params.username,req.params.unidades).then((result)=>{
    res.send(result);
  },(err)=>{
    res.send(err);
  });
});

app.put('/venta/:username/:unidades',(req,res)=>{
  venderYGuardar(req.params.username,req.params.unidades).then((resul)=>{
    res.send(resul);
  },(er)=>{
    res.send(er);
  });
});

app.listen(3030,()=>{
  console.log('Listening on port 3030');
});
