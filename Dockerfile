FROM node:latest

WORKDIR /app

ADD . /app

RUN npm install

EXPOSE 3030

CMD ["node", "Pruebas/app.js"]
